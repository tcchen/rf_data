# coding: utf-8

import sys
import datetime
import os
import pprint
import json
from collections import OrderedDict
from collections import Counter
import numpy as np
import matplotlib.pyplot as plt


def make_th_graph(tar_folder, tmstamp, app, mac, beam, title):

	plt.close('all')

	f, ax1 = plt.subplots(figsize=(12, 2))
	ax1.scatter(tmstamp, app, marker = 'o', color = beam, label = 'app')
	ax1.scatter(tmstamp, mac, marker = 'x', color = beam, label = 'mac')

	plt.xlabel('timestamp (sec)')
	plt.ylabel('Thrupt(Mbps)')

	axes = plt.gca()
	axes.set_ylim([0.0, 1100.0])

	ax1.set_title('Thrupt (' + title + ' sweep)')

	# Fine-tune figure; make subplots close to each other and hide x ticks for
	# all but bottom plot.
	# f.subplots_adjust(hspace=0)
	# plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)

	plt.grid()
	f.savefig('log/' + tar_folder + '/thrupt.png')   # save uhe figure to file
	plt.close(f)

def make_loss_graph(tar_folder, tmstamp, loss, beam, title):

	plt.close('all')

	f, ax1 = plt.subplots(figsize=(12, 2))
	ax1.scatter(tmstamp, loss, color = beam)

	plt.xlabel('timestamp (sec)')
	plt.ylabel('loss rate')

	axes = plt.gca()
	axes.set_ylim([0.0, 1.0])

	# plt.xticks(x, xlabels, rotation='vertical')

	ax1.set_title('Loss Rate (' + title + ' sweep)')

	# Fine-tune figure; make subplots close to each other and hide x ticks for
	# all but bottom plot.
	# f.subplots_adjust(hspace=0)
	# plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)

	plt.grid()
	plt.legend()
	f.savefig('log/' + tar_folder + '/loss.png')   # save uhe figure to file
	plt.close(f)

def make_rssi_graph(tar_folder, role, tmstamp, rssi, rcpi, beam, title):

	plt.close('all')

	f, ax1 = plt.subplots(figsize=(24, 5))
	ax1.scatter(tmstamp, rssi, marker = 'o', color = beam, label = 'rssi')
	ax1.scatter(tmstamp, rcpi, marker = 'x', color = beam, label = 'rcpi')

	plt.xlabel('timestamp (sec)')
	plt.ylabel('RSSI (dBm)')

	axes = plt.gca()
	axes.set_ylim([-80,-50])
	# plt.xticks(x, xlabels, rotation='vertical')

	ax1.set_title('RSSI (dBm) at ' + role + ' (' + title + ' sweep)')

	# Fine-tune figure; make subplots close to each other and hide x ticks for
	# all but bottom plot.
	# f.subplots_adjust(hspace=0)
	# plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)

	plt.grid(True, which='both')
	plt.grid(b=True, which='minor', linestyle='-', alpha=0.2)
	plt.minorticks_on()
	plt.legend()
	f.savefig('log/' + tar_folder + '/rssi_' + role + '.png')   # save the figure to file
	plt.close(f)

def get_x_ys(j_dict, title):

	rx_app_thrupt = []
	rx_mac_thrupt = []
	rx_rssi = []
	rx_rcpi = []
	tx_rssi = []
	tx_rcpi = []
	tx_loss = []

	rx_tmstamp = []
	tx_tmstamp = []

	rx_title = []
	tx_title = []

	for r_rf in j_dict['rx']:

		rx_tmstamp.append(round(float(r_rf['basic']['timestamp']/1e9), 1) - 1750)
		rx_app_thrupt.append(r_rf['basic']['Throughput(Mbps)'])
		rx_mac_thrupt.append(r_rf['MAC']['Throughput(Mbps)'])
		rx_rssi.append(r_rf['PHY']['Rx_RSSI(dBm)'])
		rx_rcpi.append(r_rf['PHY']['Rx_RCPI(dBm)'])
		rx_title.append(r_rf['basic'][title])
		
	for t_rf in j_dict['tx']:

		tx_tmstamp.append(round(float(t_rf['basic']['timestamp']/1e9), 1) - 1750)
		tx_rssi.append(t_rf['PHY']['Rx_RSSI(dBm)'])
		tx_rcpi.append(t_rf['PHY']['Rx_RCPI(dBm)'])
		tx_loss.append(t_rf['MAC']['loss_rate'])
		tx_title.append(t_rf['basic'][title])

	print('rx_app_thrupt {}'.format(len(rx_app_thrupt))) 
	print('rx_mac_thrupt {}'.format(len(rx_mac_thrupt)))
	print('rx_rssi {}'.format(len(rx_rssi)))
	print('rx_rcpi {}'.format(len(rx_rcpi)))
	print('tx_rssi {}'.format(len(tx_rssi)))
	print('tx_rcpi {}'.format(len(tx_rcpi)))
	print('tx_loss {}'.format(len(tx_loss)))
	print('rx_tmstamp {}'.format(len(rx_tmstamp)))
	print('tx_tmstamp {}'.format(len(tx_tmstamp)))
	print('rx_title {}'.format(len(rx_title)))
	print('tx_title {}'.format(len(tx_title)))

	return {

		'rx_app_thrupt': rx_app_thrupt, 
		'rx_mac_thrupt': rx_mac_thrupt,
		'rx_rssi': rx_rssi,
		'rx_rcpi': rx_rcpi,
		'tx_rssi': tx_rssi,
		'tx_rcpi': tx_rcpi,
		'tx_loss': tx_loss,
		'rx_tmstamp': rx_tmstamp,
		'tx_tmstamp': tx_tmstamp,
		'rx_title': rx_title,
		'tx_title': tx_title

	}

	
def main():

	tar_folder = sys.argv[1]

	title = sys.argv[2]

	with open('log/' + tar_folder + '/' + tar_folder + '.json', 'r') as json_in:
		data = json.load(json_in)

	infos = get_x_ys(data, title)

	color_vec = [plt.cm.rainbow(int(x*256./10)) for x in range(10)]

	infos['tx_title'] = [color_vec[i - 1] for i in infos['tx_title']]
	infos['rx_title'] = [color_vec[i - 1] for i in infos['rx_title']]

	make_th_graph(tar_folder, infos['rx_tmstamp'], infos['rx_app_thrupt'], infos['rx_mac_thrupt'], infos['rx_title'], title)
	make_loss_graph(tar_folder, infos['tx_tmstamp'], infos['tx_loss'], infos['tx_title'], title)
	make_rssi_graph(tar_folder, 'rx', infos['rx_tmstamp'], infos['rx_rssi'], infos['rx_rcpi'], infos['rx_title'], title)
	make_rssi_graph(tar_folder, 'tx', infos['tx_tmstamp'], infos['tx_rssi'], infos['tx_rcpi'], infos['tx_title'], title)

if __name__ == '__main__':
	main()
