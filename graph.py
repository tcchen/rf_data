# coding: utf-8

import sys
import datetime
import os
import pprint
import json
from collections import OrderedDict
from collections import Counter
import numpy as np
import matplotlib.pyplot as plt


def make_th_graph(tar_folder, rx_app, rx_mac, tbeam, title):
	x = range(10)
	plt.close('all')

	f, ax1 = plt.subplots(figsize=(12, 2))
	ax1.plot(x, rx_app, linestyle = '--', color = 'r', label = 'rx_app')
	ax1.plot(x, rx_mac, linestyle = '-', color = 'r', label = 'rx_mac')

	plt.xlabel('rx beam')
	plt.ylabel('Thrupt(Mbps)')

	axes = plt.gca()
	axes.set_ylim([0.0, 1200.0])

	xlabels = [str(i) for i in range(1,11)]

	plt.xticks(x, xlabels, rotation='vertical')

	ax1.set_title('Throughput (Mbps) for {}'.format(title))

	plt.title('tx beam = ' + str(tbeam), loc='right')
	# Fine-tune figure; make subplots close to each other and hide x ticks for
	# all but bottom plot.
	# f.subplots_adjust(hspace=0)
	# plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)

	plt.grid()
	plt.legend()
	f.savefig('log/' + tar_folder + '/thrupt_' + str(tbeam) + '.png')   # save uhe figure to file
	plt.close(f)

def make_loss_graph(tar_folder, loss, tbeam, title):
	x = range(10)
	plt.close('all')

	f, ax1 = plt.subplots(figsize=(12, 2))
	ax1.plot(x, loss, linestyle = '-', color = 'b')

	plt.xlabel('rx beam')
	plt.ylabel('loss rate')

	axes = plt.gca()
	axes.set_ylim([0.0, 1.0])

	xlabels = [str(i) for i in range(1,11)]

	plt.xticks(x, xlabels, rotation='vertical')

	ax1.set_title('Loss Rate for {}'.format(title))

	plt.title('tx beam = ' + str(tbeam), loc='right')
	# Fine-tune figure; make subplots close to each other and hide x ticks for
	# all but bottom plot.
	# f.subplots_adjust(hspace=0)
	# plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)

	plt.grid()
	plt.legend()
	f.savefig('log/' + tar_folder + '/loss_' + str(tbeam) + '.png')   # save uhe figure to file
	plt.close(f)

def make_rssi_graph(tar_folder, rssi, rcpi, tbeam, role, title):

	x = range(10)
	plt.close('all')

	f, ax1 = plt.subplots(figsize=(12, 2))
	ax1.plot(x, rssi, 'ro', marker = 'o', color = 'r', label = 'rx_rssi')
	ax1.plot(x, rcpi, 'ro', marker = 'x', color = 'r', label = 'rx_rcpi')

	plt.xlabel('rx beam')
	plt.ylabel('RSSI (dBm)')

	axes = plt.gca()
	axes.set_ylim([-80,-30])
	xlabels = [str(i) for i in range(1,11)]
	plt.xticks(x, xlabels, rotation='vertical')

	ax1.set_title('RSSI (dBm) at {} for {}'.format(role, title))

	plt.title('tx beam = ' + str(tbeam), loc='right')
	# Fine-tune figure; make subplots close to each other and hide x ticks for
	# all but bottom plot.
	# f.subplots_adjust(hspace=0)
	# plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)

	plt.grid(True, which='both')
	plt.grid(b=True, which='minor', linestyle='-', alpha=0.2)
	plt.minorticks_on()
	plt.legend()
	f.savefig('log/' + tar_folder + '/' + role + '_rssi_' + str(tbeam) + '.png')   # save the figure to file
	plt.close(f)


def set_title(option):

	tx = 0
	rx = 0

	if int(option[0]) + int(option[1]) == 2:
		tx = 'all'

	elif int(option[0]) + int(option[1]) == 1:
		if option[0] == '1':
			tx = 'txss'
		else:
			tx = 'rxss'
			
	if int(option[2]) + int(option[3]) == 2:
		rx = 'all'

	elif int(option[2]) + int(option[3]) == 1:
		if option[2] == '1':
			rx = 'txss'
		else:
			rx = 'rxss'

	if tx == 0:
		return 'rx {} sweeping'.format(rx)

	elif rx == 0:
		return 'tx {} sweeping'.format(tx)

	else:
		return 'tx {} + rx {} sweeping'.format(tx, rx)
			
def main():

	tar_folder = sys.argv[1]

	print('log/' + tar_folder + '/' + tar_folder + '_info.json')
	with open('log/' + tar_folder + '/' + tar_folder + '_info.json', 'r') as json_in:
		data = json.load(json_in)

	title = set_title(data['sw_option'])

	for i in range(10):
		make_th_graph(tar_folder, data['rx_app_thrupt'][i * 10: (i + 1) * 10], data['rx_mac_thrupt'][i * 10: (i + 1) * 10], i + 1, title)
		make_loss_graph(tar_folder, data['tx_mac_loss'][i * 10: (i + 1) * 10], i + 1, title)
		make_rssi_graph(tar_folder, data['tx_rssi'][i * 10: (i + 1) * 10], data['tx_rcpi'][i * 10: (i + 1) * 10], i + 1, 'tx', title)
		make_rssi_graph(tar_folder, data['rx_rssi'][i * 10: (i + 1) * 10], data['rx_rcpi'][i * 10: (i + 1) * 10], i + 1, 'rx', title)

if __name__ == '__main__':
	main()
		  
		  
		  
		  
		  
		  
		  
		  
