# coding: utf-8

import datetime
import os
import pprint
import json
import sys
import math
from collections import OrderedDict
from collections import Counter
#import matplotlib.pyplot as plt


def usage_msg():

	print('usage: python3 ./getInfo_time.py XXXX_Y')
	print('\tXXXX_Y: target file')

def roundup(a, digits=0):
	n = 10**-digits
	return round(math.ceil(a / n) * n, digits)

def get_rf(raw_rf):

	tm_rx = []
	tm_tx = []
	beam_pair_tx = []
	beam_pair_rx = []
	thrupt = []
	mac_thrupt = []
	loss = []
	rssi_rx = []
	rssi_tx = []


	# data from STA
	last_beam_pair = '_'
	rx_base = roundup(raw_rf['rx'][0]['basic']['timestamp'] / 1e9, 0)
	for index, a_rf in enumerate(raw_rf['rx']):

		if a_rf['basic']['tx_txss'] == a_rf['basic']['tx_rxss'] or a_rf['basic']['rx_txss'] == a_rf['basic']['rx_rxss']:
			last_beam_pair = str(a_rf['basic']['tx_txss']) + '_' + str(a_rf['basic']['rx_txss'])

		beam_pair_rx.append(last_beam_pair)

		thrupt.append(a_rf['basic']['Throughput(Mbps)'])
		
		tm_rx.append(roundup(a_rf['basic']['timestamp'] / 1e9, 1) - rx_base)
		mac_thrupt.append(a_rf['MAC']['Throughput(Mbps)'])
		rssi_rx.append(a_rf['PHY']['Rx_RSSI(dBm)'])

	# data from AP
	tx_base = roundup(raw_rf['tx'][0]['basic']['timestamp'] / 1e9, 0)
	for index, a_rf in enumerate(raw_rf['tx']):

		if a_rf['basic']['tx_txss'] == a_rf['basic']['tx_rxss'] or a_rf['basic']['rx_txss'] == a_rf['basic']['rx_rxss']:
			last_beam_pair = str(a_rf['basic']['tx_txss']) + '_' + str(a_rf['basic']['rx_txss'])

		beam_pair_tx.append(last_beam_pair)

		tm_tx.append(roundup(a_rf['basic']['timestamp'] / 1e9, 1) - tx_base)
		loss.append(a_rf['MAC']['loss_rate'])
		rssi_tx.append(a_rf['PHY']['Rx_RSSI(dBm)'])

	return {
		'beam_pair_tx': beam_pair_tx,
		'beam_pair_rx': beam_pair_rx,
		'thrupt': thrupt,
		'mac_thrupt': mac_thrupt,
		'loss': loss,
		'rssi_rx': rssi_rx,
		'rssi_tx': rssi_tx,
		'tm_tx': tm_tx,
		'tm_rx': tm_rx
	}
  	

def main():
	'''	
	if len(sys.argv) != 2:
		usage_msg()
		return -1
	'''
	tar_d = sys.argv[1] # get the target date
	tar_f = sys.argv[2] # get the target date

	# open json file in target folder that store rf data in a round
	#names = ['log', tar_f, tar_f + '.json']
	#with open('/'.join(names), 'r') as jfile:
	with open(tar_d + tar_f + '.json', 'r') as jfile:
		rf_data = json.load(jfile)

	# process the rf data and get rf info that required to draw graph
	info = get_rf(rf_data)
	print(info)

	#names[2] = tar_f + '_info.json'
	with open(tar_d + tar_f + '_info.json',  'w') as outf:
		json.dump(info, outf)


if __name__ == '__main__':
	main()
