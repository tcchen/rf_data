# coding: utf-8

import sys
import datetime
import os
import pprint
import json
from collections import OrderedDict
from collections import Counter
import numpy as np
import matplotlib.pyplot as plt

def usage():
	print('python3 graph_beam.py [tx/rx] [rssi/AP_beam/STA_beam] $tar1 $tar2 ...')


def make_rssi_graph(role, tar_folder, beam, rssi, label, index):

	if role == 'rx':
		role_name = 'STA'
	else:
		role_name = 'AP'

	plt.close('all')

	f, ax1 = plt.subplots()

	colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']
	markers = ['+', '.', '1', 'X', 'x', '2', '3']

	times = len(rssi)
	for i in range(times):
		ax1.plot(beam, rssi[i], color = colors[i], marker=markers[i], linestyle=':', label = '#' + index[i])

	if beam[0] > beam[1]:
		plt.xlim(beam[0] + 1, beam[-1] - 1)
	else:
		plt.xlim(beam[0] - 1, beam[-1] + 1)

    
	plt.yticks(np.arange(-80, -60, 2.0))
	ax1.set_xlabel(role_name + ' beam index')
	ax1.set_ylabel(role_name + ' RSSI(dBm)')

	box = ax1.get_position()
	ax1.set_position([box.x0, box.y0, box.width * 0.8, box.height])


	ax1.legend(loc = 'center left', title = label, bbox_to_anchor = (1, 0.5))

	ax1.set_title(role_name + '_beam_sweeping_rssi')

	# Fine-tune figure; make subplots close to each other and hide x ticks for
	# all but bottom plot.
	# f.subplots_adjust(hspace=0)
	# plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)

	plt.grid()
	f.savefig('log/' + tar_folder + '/' + role_name + '_sweeping_rssi_' + '_'.join(index) + '.png')   # save uhe figure to file
	plt.close(f)

def make_loss_graph(role, tar_folder, beam, thrupt, loss, label, index):

	plt.close('all')

	if role == 'rx':
		role_name = 'STA'
	else:
		role_name = 'AP'

	f, ax1 = plt.subplots(figsize=(10,5))
	ax2 = ax1.twinx()

	colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']
	markers = ['+', '.', '1', 'X', 'x', '2', '3']
	lines = [':', '-', '']

	lns = []
	times = len(loss)

	for i in range(times):
		lns1 = ax1.plot(beam, thrupt[i], color = colors[i], marker = markers[i], linestyle = lines[0], label = 'Thrupt for #' + index[i])
		lns2 = ax2.plot(beam, loss[i], color = colors[i], linewidth = 2.0,marker = markers[i], linestyle = lines[1], label = 'loss rate for #' + index[i])
		lns = lns + lns1 + lns2

	labs = [l.get_label() for l in lns]

	box = ax1.get_position()
	ax1.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
	ax1.legend(lns, labs, loc='upper center',bbox_to_anchor=(0.5, -0.05), title = label, ncol = times)
	
	if beam[0] > beam[1]:
		plt.xlim(beam[0] + 1, beam[-1] - 1)
	else:
		plt.xlim(beam[0] - 1, beam[-1] + 1)

	ax1.set_ylim(-20, 900)
	ax2.set_ylim(-0.05, 1.05)
	ax1.set_xlabel(role_name + ' beam index')
	ax1.set_ylabel('Throughput (Mbps)')
	ax2.set_ylabel('loss rate')

	#axes = plt.gca()

	ax1.set_title(role_name + '_beam_sweeping_loss_thrupt')

	# Fine-tune figure; make subplots close to each other and hide x ticks for
	# all but bottom plot.
	# f.subplots_adjust(hspace=0)
	# plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)

	plt.grid()
	f.savefig('log/' + tar_folder + '/' + role_name + '_sweeping_loss_thrupt_' + '_'.join(index) + '.png')   # save uhe figure to file
	plt.close(f)
	
def main():

	label = sys.argv[2]

	datas = []

	for i in range(3, len(sys.argv)):
		with open('log/' + sys.argv[i] + '/' + sys.argv[i]+ '_info.json', 'r') as json_in:
			datas.append(json.load(json_in))

	thrupts = [a['thrupt'] for a in datas]
	losses = [a['loss'] for a in datas]
	rssis = [a['rssi'] for a in datas]
	beam = datas[0]['beam']
	mcses = [str(a['mcs']) for a in datas]
	other_beams = [str(a['other_beam']) for a in datas]

	if label == 'mcs':
		index = mcses
	else:
		index = other_beams
		
	#make_rssi_graph(sys.argv[1], sys.argv[3], beam, rssis, label, index)
	make_loss_graph(sys.argv[1], sys.argv[3], beam, thrupts, losses, label, index)

if __name__ == '__main__':
	main()
