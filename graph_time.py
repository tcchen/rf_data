# coding: utf-8

import sys
import datetime
import os
import pprint
import json
from collections import OrderedDict
from collections import Counter
import numpy as np
import matplotlib.pyplot as plt

def usage():

	print('python3 graph_time.py [$target time name] A B C D')
	print('\tA: consider range the index of list of \'tm_tx\' start')
	print('\tB: consider range the index of list of \'tm_tx\' stop')
	print('\tC: consider range the index of list of \'tm_rx\' start')
	print('\tD: consider range the index of list of \'tm_rx\' stop')

#def make_graph(tar_folder, beam_pair, tmstamp, data, data_name, type_data, s_index, e_index):
def make_graph(tar_folder, beam_pair, tmstamp, data, data_name, type_data):

	plt.close('all')

	f, ax1 = plt.subplots()

	colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']

	for i in range(len(beam_pair)):
		ax1.plot(tmstamp[i], data[i], color = colors[i], label = beam_pair[i])

	plt.xlabel('timestamp (sec)')
	plt.ylabel(data_name)

	plt.xticks(np.arange(min(tmstamp[0]) - 0.1, max(tmstamp[-1]) + 0.1, 1.0))

	'''
	if len(tmstamp[0]) > e_index:
		plt.xticks(np.arange(tmstamp[0][s_index] - 0.1, tmstamp[0][e_index] + 0.1, 0.1))
		ax1.set_xlim([tmstamp[0][s_index] - 0.1, tmstamp[0][e_index] + 0.1])
	else:
		plt.xticks(np.arange(tmstamp[0][s_index] - 0.1, tmstamp[1][e_index - len(tmstamp[0])] + 0.1, 0.1))
		ax1.set_xlim([tmstamp[0][s_index] - 0.1, tmstamp[1][e_index - len(tmstamp[0])] + 0.1])
	'''
	if type_data == 'rssi':
		print('this is for rssi')
		ax1.set_ylim([-80, -60])
		plt.yticks(np.arange(-80, -60,  5))
		'''if len(data) == 1:
			plt.yticks(np.arange(min(data[0]) - 1, max(data[0]) + 1, 1.0))
		else:
			plt.yticks(np.arange(min(data[0], data[1]) - 1, max(data[0], data[1]) + 1, 1.0))'''
	elif type_data == 'thrupt':
		print('this is for thrupt')
		ax1.set_ylim([0, 900])
		plt.yticks(np.arange(0, 900, 100))
	elif type_data == 'loss':
		print('this is for loss')
		ax1.set_ylim([0, 1])
		plt.yticks(np.arange(0, 1, 0.2))
		
	ax1.set_title(data_name)

	# Fine-tune figure; make subplots close to each other and hide x ticks for
	# all but bottom plot.

	plt.grid()
	ax1.legend(loc=2)
	f.savefig(tar_folder + data_name + '.png')   # save uhe figure to file
	plt.close(f)


def sub_sort(a_lst, beam_pair):

	last_b = beam_pair[0]
	final = []
	tmp = []

	for i, value in enumerate(a_lst):

		tmp.append(value)
		last_b = beam_pair[i]

		if i == len(a_lst) - 1:
			final.append(tmp)
		elif last_b != beam_pair[i + 1]:
			# the last of this beam pair group
			final.append(tmp)
			tmp = []

	print(final)

	return final

def all_beam(beam_lst):

	tmp = []
	for item in beam_lst:
		if item not in tmp:
			tmp.append(item)

	return tmp

def sort(dict_info):

	return {
		'beam_pair_tx': all_beam(dict_info['beam_pair_tx']),
		'beam_pair_rx': all_beam(dict_info['beam_pair_rx']),
		'tm_tx': sub_sort(dict_info['tm_tx'], dict_info['beam_pair_tx']),
		'tm_rx': sub_sort(dict_info['tm_rx'], dict_info['beam_pair_rx']),
		'rssi_rx': sub_sort(dict_info['rssi_rx'], dict_info['beam_pair_rx']),
		'rssi_tx': sub_sort(dict_info['rssi_tx'], dict_info['beam_pair_tx']),
		'thrupt': sub_sort(dict_info['thrupt'], dict_info['beam_pair_rx']),
		'mac_thrupt': sub_sort(dict_info['mac_thrupt'], dict_info['beam_pair_rx']),
		'loss': sub_sort(dict_info['loss'], dict_info['beam_pair_tx'])
	}

def main():

	'''
	if len(sys.argv) != 6:
		usage()
		break
	'''

	tar_folder = sys.argv[1]
	tar_file = sys.argv[2]

	#t_s_index = int(sys.argv[2])
	#t_e_index = int(sys.argv[3])
	#r_s_index = int(sys.argv[4])
	#r_e_index = int(sys.argv[5])

	#with open('log/0510/' + tar_folder + '/' + tar_folder + '_info.json', 'r') as json_in:
	with open(tar_folder + tar_file, 'r') as json_in:
		data = json.load(json_in)

	data['tm_tx'] = [round(i,2) for i in data['tm_tx']]
	data['tm_rx'] = [round(i,2) for i in data['tm_rx']]

	infos = sort(data)

	#make_graph(tar_folder, infos['beam_pair_rx'], infos['tm_rx'], infos['thrupt'], 'App_Throughput(Mbps)', 'thrupt', r_s_index, r_e_index)
	#make_graph(tar_folder, infos['beam_pair_rx'], infos['tm_rx'], infos['mac_thrupt'], 'MAC_Throughput(Mbps)', 'thrupt', r_s_index, r_e_index)
	#make_graph(tar_folder, infos['beam_pair_rx'], infos['tm_rx'], infos['rssi_rx'], 'RSSI_sta', 'rssi', r_s_index, r_e_index)
	#make_graph(tar_folder, infos['beam_pair_tx'], infos['tm_tx'], infos['rssi_tx'], 'RSSI_ap', 'rssi', t_s_index, t_e_index)
	#make_graph(tar_folder, infos['beam_pair_tx'], infos['tm_tx'], infos['loss'], 'loss_rate', 'loss', t_s_index, t_e_index)

	make_graph(tar_folder, infos['beam_pair_rx'], infos['tm_rx'], infos['thrupt'], 'App_Throughput(Mbps)', 'thrupt')
	make_graph(tar_folder, infos['beam_pair_rx'], infos['tm_rx'], infos['mac_thrupt'], 'MAC_Throughput(Mbps)', 'thrupt')
	make_graph(tar_folder, infos['beam_pair_rx'], infos['tm_rx'], infos['rssi_rx'], 'RSSI_sta', 'rssi')
	make_graph(tar_folder, infos['beam_pair_tx'], infos['tm_tx'], infos['rssi_tx'], 'RSSI_ap', 'rssi')
	make_graph(tar_folder, infos['beam_pair_tx'], infos['tm_tx'], infos['loss'], 'loss_rate', 'loss')
if __name__ == '__main__':
	main()
