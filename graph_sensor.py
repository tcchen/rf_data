# coding: utf-8

import sys
import datetime
import os
import pprint
import json
from collections import OrderedDict
from collections import Counter
import numpy as np
import matplotlib.pyplot as plt

SMALL_SIZE = 14
MEDIUM_SIZE = 14
BIGGER_SIZE = 18

def make_graph(raw, event, time, event_name, direction):

    plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
    plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
    plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
    plt.close('all')
    
    f, ax1 = plt.subplots()
    #ax2 = ax1.twinx()

    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']
    markers = ['+', '.', '1', 'X', 'x', '2', '3']
    lines = [':', '-', '']

    lns = []

    if event_name == 'X':
        lns1 = ax1.plot(time, raw, color = colors[0], label = 'X-raw')
    #lns3 = ax2.plot(time, event, color = colors[2], linewidth = 3.0, label = 'Event on ' + event_name)
    else:
        lns1 = ax1.plot(time, raw, color = colors[1], label = 'Y-raw')
    lns3 = ax1.plot(time, event, color = colors[2], linewidth = 3.0, label = 'Event on ' + event_name)
    lns = lns + lns1 + lns3

    labs = [l.get_label() for l in lns]

    box = ax1.get_position()
    ax1.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
    #ax1.legend(lns, labs, loc='upper center',bbox_to_anchor=(0.5, -0.05))
    ax1.legend(lns, labs)

    #ax1.set_ylim(-20, 900)
    #ax2.set_ylim(-1.2, 1.2)
    ax1.set_xlabel('frame')
    ax1.set_ylabel('data')
    #ax2.set_ylabel('event')

    ax1.set_yticks(np.arange(-3, 4, step=1))
    #ax2.set_yticks(np.arange(-1, 2, step=1))
    #axes = plt.gca()

    ax1.set_title('Event detection on ' + direction)

    # Fiddne-tune figure; make subplots close to each other and hide x ticks for
    # all but bottom plot.
    # f0.subplots_adjust(hspace=0)
    # plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)

    plt.grid()
    f.savefig('Event_graph_' + direction + '.png')   # save uhe figure to file
    plt.close(f)


def parse_line(lines):

    x_raw = []
    y_raw = []
    x_ev = []
    y_ev = []

    for line in lines:
        tmp = line.split(':')
        x_raw.append(float(tmp[1].split(',')[0]))
        y_raw.append(float(tmp[2].split(',')[0]))
        x_ev.append(int(tmp[3].split(',')[0]))
        y_ev.append(int(tmp[4].split(',')[0]))

    print(x_raw)
    print(y_raw)
    print(x_ev)
    print(y_ev)

    return [x_raw, y_raw, x_ev, y_ev]

def main():
    
    start = int(sys.argv[1])
    end = int(sys.argv[2])
    
    direction = sys.argv[3]
    axis = sys.argv[4]

    with open('result.log', 'r') as fp:
        all_raw = fp.readlines()
        
    raw = [line for i, line in enumerate(all_raw) if i >= start - 1 and i <= end - 1]
    time = [i for i in range(end - start + 1)]

    data = parse_line(raw)

    if sys.argv[3] == 'Y':
        make_graph(data[1], data[3], time, sys.argv[3], sys.argv[4])
    else:
        make_graph(data[0], data[2], time, sys.argv[3], sys.argv[4])

if __name__ == '__main__':
	main()
