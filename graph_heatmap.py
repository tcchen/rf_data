import matplotlib.pyplot as plt
import numpy as np
import sys
import json

def make_graph(title, data_list, save_path):

	column_labels = list(range(1,11))
	row_labels = list(range(1,11))
	data = np.array([
					data_list[0],
					data_list[1],
					data_list[2],
					data_list[3],
					data_list[4],
					data_list[5],
					data_list[6],
					data_list[7],
					data_list[8],
					data_list[9],
					])

	fig, axis = plt.subplots() # il me semble que c'est une bonne habitude de faire supbplots
	heatmap = axis.pcolor(data, cmap=plt.cm.Blues) # heatmap contient les valeurs

	axis.set_yticks(np.arange(data.shape[0])+0.5, minor=False)
	axis.set_xticks(np.arange(data.shape[1])+0.5, minor=False)

	axis.invert_yaxis()

	axis.set_yticklabels(row_labels, minor=False)
	axis.set_xticklabels(column_labels, minor=False)

	axis.set_title(title)

	plt.xlabel('STA sector ID')
	plt.ylabel('AP sector ID')

	plt.colorbar(heatmap)

	fig.set_size_inches(10, 10)

	plt.savefig(save_path + title + '.png', dpi=100)


def ch_format(j_dict):

	tmp_dict = {}

	for key, value in j_dict.items():

		if key == 'sw_option':
			continue
		tmp_dict[key] = []

		for i in range(0, 10):
			tmp_dict[key].append([])
			for j in range(0, 10):
				tmp_dict[key][i].append(value[i * 10 + j])

		print(key)

	return tmp_dict

def main():

	tar_folder = sys.argv[1]

	with open('log/' + tar_folder + '/' + tar_folder + '_info.json', 'r') as json_in:
		data = json.load(json_in)

	infos = ch_format(data)
	path = 'log/' + tar_folder + '/'

	for key, value in infos.items():
		make_graph(key, value, path)


if __name__ == '__main__':
	main()
