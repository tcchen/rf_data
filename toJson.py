# coding: utf-8

import datetime
import os
import pprint
import json
from collections import OrderedDict
from collections import Counter
#import matplotlib.pyplot as plt
import sys

def RF_data():

    # basic info
    role = ""
    tx_beam = 0
    rx_beam = 0
    date = ""
    timestamp = 0
    app_thrupt = 0

    # PHY COUNTERS:
    PHY_tx = 0
    PHY_rx = 0
    PHY_rx_cp_pkt = 0
    PHY_rx_sc_pkt = 0
    PHY_PER = 0
    PHY_rx_STF = 0
    PHY_rx_HCS = 0
    PHY_rx_FCS = 0

    PHY_rx_EVM = 0
    PHY_rx_SNR = 0
    PHY_rx_RSSI = 0
    PHY_rx_RCPI = 0
    PHY_AGC_GAIN = 0

    PHY_mcs = 0
    # MCS COUNTERS:
    MAC_thrpt = 0
    MAC_loss_rate = 0
    MAC_tx = 0
    MAC_rx = 0
    MAC_fail = 0
    MAC_ack = 0
    MAC_tx_done = 0

    new_dict = {
        'basic': {
            'role': role,
            'tx_beam': tx_beam,
            'rx_beam': rx_beam,
            'date': date,
            'timestamp': timestamp,
            'Throughput(Mbps)': app_thrupt
        },
        'PHY': {
            'Tx_Count': PHY_tx,
            'Rx_Count': PHY_rx,
            'Rx_CP_PKT': PHY_rx_cp_pkt,
            'Rx_SC_PKT': PHY_rx_sc_pkt,
            'PER': PHY_PER,
            'Rx_STF': PHY_rx_STF,
            'Rx_HCS': PHY_rx_HCS,
            'Rx_FCS': PHY_rx_FCS,
            'Rx_EVM(dBm)': PHY_rx_EVM,
            'Rx_SNR(dBm)': PHY_rx_SNR,
            'Rx_RSSI(dBm)': PHY_rx_RSSI,
            'Rx_RCPI(dBm)': PHY_rx_RCPI,
            'Rx_AGC_GAIN': PHY_AGC_GAIN,
            'MCS': PHY_mcs
        },
        'MAC': {
            'Throughput(Mbps)': MAC_thrpt,
            'loss_rate': MAC_loss_rate,
            'Total_Tx': MAC_tx,
            'Total_Rx': MAC_rx,
            'Total_Fail': MAC_fail,
            'Total_Ack': MAC_ack,
            'Total_Tx_Done': MAC_tx_done
        }
    }

    return new_dict

# transfer the format into datetime.datetime

def get_time(time_str):
    t = datetime.datetime.strptime(time_str[4:], '%b %d %H:%M:%S %Y')
    return t.strftime("%Y-%m-%d %H:%M:%S")

def record_RF(rf_list):

    # put rf data list into a class
    rf_data = RF_data()

    for i,s in enumerate(rf_list):
        tmp = s.split(': ', 1)[1]
        rf_list[i] = tmp.split('\n', 1)[0]

    rf_data['basic']['role'] = rf_list[0]
    rf_data['basic']['tx_beam'] = int(rf_list[1])
    rf_data['basic']['rx_beam'] = int(rf_list[2])
    rf_data['basic']['date'] = get_time(rf_list[3])
    rf_data['basic']['timestamp'] = int(rf_list[4].split(' ', 1)[0])
    rf_data['basic']['Throughput(Mbps)'] = float(rf_list[5])

    rf_data['PHY']['Tx_Count'] = int(rf_list[6])
    rf_data['PHY']['Rx_Count'] = int(rf_list[7])
    rf_data['PHY']['Rx_CP_PKT'] = int(rf_list[8])
    rf_data['PHY']['Rx_SC_PKT'] = int(rf_list[9])
    rf_data['PHY']['PER'] = float(rf_list[10])
    rf_data['PHY']['Rx_STF'] = float(rf_list[11])
    rf_data['PHY']['Rx_HCS'] = float(rf_list[12])
    rf_data['PHY']['Rx_FCS'] = float(rf_list[13])
    rf_data['PHY']['Rx_EVM(dBm)'] = int(rf_list[14])
    rf_data['PHY']['Rx_SNR(dBm)'] = int(rf_list[15])
    rf_data['PHY']['Rx_RSSI(dBm)'] = int(rf_list[16])
    rf_data['PHY']['Rx_RCPI(dBm)'] = int(rf_list[17])
    rf_data['PHY']['Rx_AGC_GAIN'] = int(rf_list[18])
    rf_data['PHY']['MCS'] = int(rf_list[19])

    rf_data['MAC']['Throughput(Mbps)'] = float(rf_list[20].split(' ', 1)[0])
    rf_data['MAC']['loss_rate'] = float(rf_list[21])
    rf_data['MAC']['Total_Tx'] = int(rf_list[22])
    rf_data['MAC']['Total_Rx'] = int(rf_list[23])
    rf_data['MAC']['Total_Fail'] = int(rf_list[24])
    rf_data['MAC']['Total_Ack'] = int(rf_list[25])
    rf_data['MAC']['Total_Tx_Done'] = int(rf_list[26])

    return rf_data

# get RF.log under './log/' into json
# put result and log file to $folder_name
# format of folder_name: _ _ _ _ (date) _ (id) _ (left_dongle) _ (right dongle)

def input_file(folder_name):

    __cwd = os.getcwd() + '/log/' # folder to put RF log files

    # get basic info of this time's testing from folder name
    rf_dict = {
        'info': {
            'date': folder_name[0:4],
            'ID': folder_name[4],
            'left': folder_name[5],
            'right': folder_name[6]
        }
    }

    # create dict for each beam combination
    # format: 'X_Y' X: tx beam, Y: rx beam
    for i in range(1,11):

        for j in range(1,11):

            rf_dict[str(i)+'_'+str(j)] = {
                'tx': [],
                'rx': []
            }

    # log file name
    log_files = ['0_RFdata.log', '1_RFdata.log']

    for logfile in log_files:

        with open(__cwd + logfile, 'r') as rf_file:
            rf_buf = rf_file.readlines()

        if rf_buf == []:
            continue

        tmp_list = [] # use to store a rf data

        for line in rf_buf:

            if line != '\n' and line !='PHY COUNTERS: \n' and line != 'MAC COUNTERS: \n': # not a blank line

                if line[:4] == 'Role' and len(tmp_list) > 0: # the last line of a rf data

                    # send the list to form a RF class
                    a_rf = record_RF(tmp_list)

                    # tell the beam combination and role
                    beam_com = str(a_rf['basic']['tx_beam']) + '_' + str(a_rf['basic']['rx_beam'])
                    rf_role = a_rf['basic']['role']

                    # put this rf into the list of beam comination dict of corresponding role
                    rf_dict[beam_com][rf_role].append(a_rf)

                    # clean this rf data
                    tmp_list = []

                tmp_list.append(line)

        # the last data
        # send the list to form a RF class
        a_rf = record_RF(tmp_list)

        # tell the beam combination and role
        beam_com = str(a_rf['basic']['tx_beam']) + '_' + str(a_rf['basic']['rx_beam'])
        rf_role = a_rf['basic']['role']

        # put this rf into the list of beam comination dict of corresponding role
        rf_dict[beam_com][rf_role].append(a_rf)

        # clean this rf data
        tmp_list = []

    return rf_dict


def main():
    
    '''
    if len(sys.argv) != 2:
        print('usage: python3 data_result.py $folder_name\n')
        return
    '''
    rf_dict = input_file(sys.argv[1])

    folder_name = sys.argv[1][0:4] + '_' + sys.argv[1][4]
    os.mkdir('log/' + folder_name)

    with open('log/' + folder_name + '/' + folder_name + '.json', 'w') as out:
        json.dump(rf_dict, out)

    os.rename('log/0_RFdata.log', 'log/' + folder_name + '/0_RFdata.log')
    os.rename('log/1_RFdata.log', 'log/' + folder_name + '/1_RFdata.log')

if __name__ == '__main__':
    main()
