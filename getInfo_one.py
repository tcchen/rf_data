# coding: utf-8

import datetime
import os
import pprint
import json
import sys
from collections import OrderedDict
from collections import Counter
#import matplotlib.pyplot as plt


def usage_msg():

	print('usage: python3 ./getInfo_one.py [$target folder]')

# this function can get avg of float(double) type parameter

def get_avg(thrupt):
	
	if len(thrupt) == 0:
		return 0

	# sum_thrupt = [th for th in thrupt]
	# return sum(sum_thrupt) / len(thrupt)
	return sum(thrupt) / len(thrupt)


# this function return most frequently measured value
# if more than one value with highest frequency
# return the middle of those values

def get_mode(rssi):

	data = Counter(rssi).most_common()


	# if nothing in rssi: return 0
	if len(data) == 0:
		return 0

	print(rssi)
	
	# get a list with highest show up frequency (same with the 1st data)
	highest = [a[0] for a in data if a[1] == data[0][1]]
	highest.sort()
	
	return highest[int(len(highest) / 2)]


# get a round of rf data
# take out required parameters and take avg or mode

def get_rf(raw_rf):

	tmp_tx_app_thrupt = []
	tmp_tx_mac_loss = []
	tmp_tx_mac_thrupt = []

	tmp_tx_rssi = []
	tmp_tx_rcpi = []
	tmp_tx_snr = []
		
	tmp_rx_app_thrupt = []
	tmp_rx_mac_thrupt = []
	tmp_rx_rssi = []
	tmp_rx_rcpi = []
	tmp_rx_snr = []
		
	last_r = 0
	last_t = 0
	
	for index, item in enumerate(raw_rf['tx']):
		
		tmp_tx_app_thrupt.append(item['basic']['Throughput(Mbps)'])
		tmp_tx_mac_loss.append(item['MAC']['loss_rate'])
		tmp_tx_mac_thrupt.append(item['MAC']['Throughput(Mbps)'])

		if last_t < item['MAC']['Total_Rx']:
			tmp_tx_rssi.append(item['PHY']['Rx_RSSI(dBm)'])
			tmp_tx_rcpi.append(item['PHY']['Rx_RCPI(dBm)'])
			tmp_tx_snr.append(item['PHY']['Rx_SNR(dBm)'])
			last_t = item['MAC']['Total_Rx']
		
	for index, item in enumerate(raw_rf['rx']):
		
		tmp_rx_app_thrupt.append(item['basic']['Throughput(Mbps)'])
		tmp_rx_mac_thrupt.append(item['MAC']['Throughput(Mbps)'])
		if last_r < item['MAC']['Total_Rx']:
			tmp_rx_rssi.append(item['PHY']['Rx_RSSI(dBm)'])
			tmp_rx_rcpi.append(item['PHY']['Rx_RCPI(dBm)'])
			tmp_rx_snr.append(item['PHY']['Rx_SNR(dBm)'])
			last_r = item['MAC']['Total_Rx']
		
	
	# store avg/mode of each parameter
	tx_app_thrupt = get_avg(tmp_tx_app_thrupt)
	rx_app_thrupt = get_avg(tmp_rx_app_thrupt)
	tx_mac_loss = get_avg(tmp_tx_mac_loss)
	tx_mac_thrupt = get_avg(tmp_tx_mac_thrupt)
	rx_mac_thrupt = get_avg(tmp_rx_mac_thrupt)
	rx_rssi = get_mode(tmp_rx_rssi)
	tx_rssi = get_mode(tmp_tx_rssi)
	rx_rcpi = get_mode(tmp_rx_rcpi)
	tx_rcpi = get_mode(tmp_tx_rcpi)
	rx_snr = get_mode(tmp_rx_snr)
	tx_snr = get_mode(tmp_tx_snr)
		
	return {
			
		'tx_app_thrupt': tx_app_thrupt,
		'rx_app_thrupt': rx_app_thrupt,
		'tx_mac_loss': tx_mac_loss,
		'tx_mac_thrupt': tx_mac_thrupt,
		'rx_mac_thrupt': rx_mac_thrupt,
		'rx_rssi': rx_rssi,
		'tx_rssi': tx_rssi,
		'rx_rcpi': rx_rcpi,
		'tx_rcpi': tx_rcpi,
		'rx_snr': rx_snr,
		'tx_snr': tx_snr
	
	}


def main():
	
	if len(sys.argv) < 2:
		usage()
		return -1

	tar_f = sys.argv[1] # get the target date

	# open json file in target folder that store rf data in a round
	names = ['log', tar_f, tar_f + '.json']
	with open('/'.join(names), 'r') as jfile:
		rf_data = json.load(jfile)

	# process the rf data and get rf info that required to draw graph
	info = get_rf(rf_data)

	names[2] = tar_f + '_info.json'
	with open('/'.join(names), 'w') as outf:
		json.dump(info, outf)

	print (info);


if __name__ == '__main__':
	main()
