# coding: utf-8

import datetime
import os
import pprint
import json
import sys
from collections import OrderedDict
from collections import Counter
#import matplotlib.pyplot as plt


def usage_msg():

	print('usage: python3 ./getInfo_sw.py [tx/rx]  XXXX_Y')
	print('\tXXXX_Y: target file')

# this function return most frequently measured value
# if more than one value with highest frequency
# return the middle of those values

def get_mode(rssi):

	data = Counter(rssi).most_common()


	# if nothing in rssi: return 0
	if len(data) == 0:
		return 0

	print(rssi)
	
	# get a list with highest show up frequency (same with the 1st data)
	highest = [a[0] for a in data if a[1] == data[0][1]]
	highest.sort()
	
	return highest[int(len(highest) / 2)]


def get_loss(tx_a, ack_a, tx_b, ack_b):
	return ((tx_b - tx_a) - (ack_b - ack_a)) / (tx_b - tx_a)


def get_thrupt(rx_a, time_a, rx_b, time_b):
	return (rx_b - rx_a) * 8 * 4096 * 1e3 / (time_b - time_a)
	
# get a round of rf data
# take out required parameters and take avg or mode

def get_rf(raw_rf, role):

	beam = []
	thrupt = []
	loss = []
	rssi = []
	other_beam = 0

	last_rx = 0
	last_time = 0
	last_tx = 0
	last_ack = 0
	last_beam = 0
	rssi_tmp = []

	if role == 'rx':
		ref = 'rx_txss'
		other_beam = raw_rf['rx'][10]['basic']['tx_txss']
	else:
		ref = 'tx_txss'
		other_beam = raw_rf['rx'][10]['basic']['rx_txss']

	check_rx = 0

	# data from STA
	for index, a_rf in enumerate(raw_rf['rx']):

		if a_rf['MAC']['Total_Rx'] == 0:
			continue
		if check_rx == 0:
			check_rx = a_rf['MAC']['Total_Rx']

		if index == 0 or a_rf['basic'][ref] != last_beam:

			if last_beam > 0:
				if role == 'rx':
					rssi.append(get_mode(rssi_tmp))
					rssi_tmp = []

				thrupt.append(get_thrupt(last_rx, last_time, raw_rf['rx'][index-1]['MAC']['Total_Rx'], raw_rf['rx'][index-1]['basic']['timestamp']))

			beam.append(a_rf['basic'][ref])
			last_beam = a_rf['basic'][ref]

			last_rx = a_rf['MAC']['Total_Rx']
			last_time = a_rf['basic']['timestamp']

			if role == 'rx' and a_rf['MAC']['Total_Rx'] > check_rx:
				rssi_tmp.append(a_rf['PHY']['Rx_RSSI(dBm)'])

		else:

			if role == 'rx' and a_rf['MAC']['Total_Rx'] > check_rx:
				rssi_tmp.append(a_rf['PHY']['Rx_RSSI(dBm)'])

	# last
	if role == 'rx':
		rssi.append(get_mode(rssi_tmp))
		rssi_tmp = []

	thrupt.append(get_thrupt(last_rx, last_time, raw_rf['rx'][index-1]['MAC']['Total_Rx'], raw_rf['rx'][index-1]['basic']['timestamp']))
	last_beam = 0

	check_rx = 0
	# data from AP
	for index, a_rf in enumerate(raw_rf['tx']):

		if check_rx == 0:
			check_rx = a_rf['MAC']['Total_Rx']

		if index == 0 or a_rf['basic'][ref] != last_beam:

			if last_beam > 0:
				if role == 'tx':
					rssi.append(get_mode(rssi_tmp))
					rssi_tmp = []

				loss.append(get_loss(last_tx, last_ack, raw_rf['tx'][index-1]['MAC']['Total_Tx'], raw_rf['tx'][index-1]['MAC']['Total_Ack']))

			# beam.append(a_rf['basic'][ref])
			last_beam = a_rf['basic'][ref]

			last_tx = a_rf['MAC']['Total_Tx']
			last_ack = a_rf['MAC']['Total_Ack']

			if role == 'tx' and a_rf['MAC']['Total_Rx'] > check_rx:
				rssi_tmp.append(a_rf['PHY']['Rx_RSSI(dBm)'])

		else:
			if role == 'tx' and a_rf['MAC']['Total_Rx'] > check_rx:
				rssi_tmp.append(a_rf['PHY']['Rx_RSSI(dBm)'])

	# last
	if role == 'tx':
		rssi.append(get_mode(rssi_tmp))
		rssi_tmp = []

	loss.append(get_loss(last_tx, last_ack, raw_rf['tx'][index-1]['MAC']['Total_Tx'], raw_rf['tx'][index-1]['MAC']['Total_Ack']))

	return {
		'mcs': raw_rf['tx'][10]['PHY']['MCS'],
		'thrupt': thrupt,
		'loss': loss,
		'rssi': rssi,
		'beam': beam,
		'other_beam': other_beam
	}
  	

def main():
	
	if len(sys.argv) != 3:
		usage_msg()
		return -1

	tar_f = sys.argv[2] # get the target date

	# open json file in target folder that store rf data in a round
	names = ['log', tar_f, tar_f + '.json']
	with open('/'.join(names), 'r') as jfile:
		rf_data = json.load(jfile)

	# process the rf data and get rf info that required to draw graph
	info = get_rf(rf_data, sys.argv[1])
	print(info)

	names[2] = tar_f + '_info.json'
	with open('/'.join(names), 'w') as outf:
		json.dump(info, outf)


if __name__ == '__main__':
	main()
