# coding: utf-8

import datetime
import os
import pprint
import json
import sys
from collections import OrderedDict
from collections import Counter
#import matplotlib.pyplot as plt


def usage_msg():

	print('usage: python3 ./getInfo_new.py 1111 XXXX Y Z ..')
	print('\t_ _ _ _ for tx_txss tx_rxss rx_txss rx_rxss: 1 for sweeping 0 for fixed')
	print('\tXXXX: target date')
	print('\tY X ...: target index, can be more than one')

# this function can get avg of float(double) type parameter

def get_avg(thrupt):
	
	if len(thrupt) == 0:
		return 0

	# sum_thrupt = [th for th in thrupt]
	# return sum(sum_thrupt) / len(thrupt)
	return sum(thrupt) / len(thrupt)


# this function return most frequently measured value
# if more than one value with highest frequency
# return the middle of those values

def get_mode(rssi):

	data = Counter(rssi).most_common()


	# if nothing in rssi: return 0
	if len(data) == 0:
		return 0

	print(rssi)
	
	# get a list with highest show up frequency (same with the 1st data)
	highest = [a[0] for a in data if a[1] == data[0][1]]
	highest.sort()
	
	return highest[int(len(highest) / 2)]


# this function will sort rf data with beam pair
# use sys.argv[1] to tell fixed beam and sweep beam

def sort_with_beam_pair(rf_data):

	beamP_dicts = {}

	for i in range(1, 11):

		for j in range(1, 11):

			beamP_dicts['{}_{}'.format(i, j)] = {
				'tx': [],
				'rx': []
			}

	tx = ''
	rx = ''

	if sys.argv[1][0] == '1':
		tx = 'tx_txss'
	elif sys.argv[1][1] == '1':
		tx = 'tx_rxss'
	else:
		tx = 'tx_txss'

	if sys.argv[1][3] == '1':
		rx = 'rx_rxss'
	elif sys.argv[1][2] == '1':
		rx = 'rx_txss'
	else:
		rx = 'rx_rxss'
		
	for t_rf in rf_data['tx']:

		bpair = '{}_{}'.format(t_rf['basic'][tx], t_rf['basic'][rx])

		beamP_dicts[bpair]['tx'].append(t_rf)

	for r_rf in rf_data['rx']:

		bpair = '{}_{}'.format(r_rf['basic'][tx], r_rf['basic'][rx])

		beamP_dicts[bpair]['rx'].append(r_rf)

	return beamP_dicts

		
# get a round of rf data
# take out required parameters and take avg or mode

def get_rf(raw_rf):
	# input: a test round

	# list to store avg/mode of each parameter
	tx_app_thrupt = [0] * 100
	rx_app_thrupt = [0] * 100
	tx_mac_loss = [0] * 100
	tx_mac_thrupt = [0] * 100
	rx_mac_thrupt = [0] * 100
	rx_rssi = [0] * 100
	tx_rssi = [0] * 100
	rx_rcpi = [0] * 100
	tx_rcpi = [0] * 100
	rx_snr= [0] * 100
	tx_snr = [0] * 100

	rf_data = sort_with_beam_pair(raw_rf)

	for beam_set, RFs in rf_data.items():
		
		tmp_tx_app_thrupt = [] 
		tmp_rx_app_thrupt = [] 
		tmp_tx_mac_loss = [] 
		tmp_tx_mac_thrupt = [] 
		tmp_rx_mac_thrupt = [] 
		tmp_rx_rssi = []
		tmp_tx_rssi = []
		tmp_rx_rcpi = []
		tmp_tx_rcpi = []
		tmp_rx_snr = []
		tmp_tx_snr = []

		last_t = 0
		last_r = 0
		
		for index, item in enumerate(RFs['tx']):
			
			tmp_tx_app_thrupt.append(item['basic']['Throughput(Mbps)'])
			tmp_tx_mac_loss.append(item['MAC']['loss_rate'])
			tmp_tx_mac_thrupt.append(item['MAC']['Throughput(Mbps)'])
			if last_t < item['MAC']['Total_Rx']:
				tmp_tx_rssi.append(item['PHY']['Rx_RSSI(dBm)'])
				tmp_tx_rcpi.append(item['PHY']['Rx_RCPI(dBm)'])
				tmp_tx_snr.append(item['PHY']['Rx_SNR(dBm)'])
				last_t = item['MAC']['Total_Rx']
			
		for index, item in enumerate(RFs['rx']):
			
			tmp_rx_app_thrupt.append(item['basic']['Throughput(Mbps)'])
			tmp_rx_mac_thrupt.append(item['MAC']['Throughput(Mbps)'])
			if last_r < item['MAC']['Total_Rx']:
				tmp_rx_rssi.append(item['PHY']['Rx_RSSI(dBm)'])
				tmp_rx_rcpi.append(item['PHY']['Rx_RCPI(dBm)'])
				tmp_rx_snr.append(item['PHY']['Rx_SNR(dBm)'])
				last_r = item['MAC']['Total_Rx']
			
		tx_b = int(beam_set[:beam_set.find('_')])
		rx_b = int(beam_set[beam_set.find('_')+1:])
		
		b_set = (tx_b - 1) * 10 + (rx_b - 1)
		
		# list to store avg/mode of each parameter
		tx_app_thrupt[b_set] = get_avg(tmp_tx_app_thrupt)
		rx_app_thrupt[b_set] = get_avg(tmp_rx_app_thrupt)
		tx_mac_loss[b_set] = get_avg(tmp_tx_mac_loss)
		tx_mac_thrupt[b_set] = get_avg(tmp_tx_mac_thrupt)
		rx_mac_thrupt[b_set] = get_avg(tmp_rx_mac_thrupt)
		rx_rssi[b_set] = get_mode(tmp_rx_rssi)
		tx_rssi[b_set] = get_mode(tmp_tx_rssi)
		rx_rcpi[b_set] = get_mode(tmp_rx_rcpi)
		tx_rcpi[b_set] = get_mode(tmp_tx_rcpi)
		rx_snr[b_set] = get_mode(tmp_rx_snr)
		tx_snr[b_set] = get_mode(tmp_tx_snr)
		
	return {
			
		'tx_app_thrupt': tx_app_thrupt,
		'rx_app_thrupt': rx_app_thrupt,
		'tx_mac_loss': tx_mac_loss,
		'tx_mac_thrupt': tx_mac_thrupt,
		'rx_mac_thrupt': rx_mac_thrupt,
		'rx_rssi': rx_rssi,
		'tx_rssi': tx_rssi,
		'rx_rcpi': rx_rcpi,
		'tx_rcpi': tx_rcpi,
		'rx_snr': rx_snr,
		'tx_snr': tx_snr
	
	}


# use each info of rounds and take avg or mode

def between_index(info_list):

	# list to store avg/mode of each parameter
	tx_app_thrupt = [0] * 100
	rx_app_thrupt = [0] * 100
	tx_mac_loss = [0] * 100
	tx_mac_thrupt = [0] * 100
	rx_mac_thrupt = [0] * 100
	rx_rssi = [0] * 100
	tx_rssi = [0] * 100
	rx_rcpi = [0] * 100
	tx_rcpi = [0] * 100
	rx_snr = [0] * 100
	tx_snr = [0] * 100

	for i in range(100):

		tx_app_thrupt[i] = get_avg([a_info['tx_app_thrupt'][i] for a_info in info_list])
		rx_app_thrupt[i] = get_avg([a_info['rx_app_thrupt'][i] for a_info in info_list])
		tx_mac_loss[i] = get_avg([a_info['tx_mac_loss'][i] for a_info in info_list])
		tx_mac_thrupt[i] = get_avg([a_info['tx_mac_thrupt'][i] for a_info in info_list])
		rx_mac_thrupt[i] = get_avg([a_info['rx_mac_thrupt'][i] for a_info in info_list])
		rx_rssi[i] = get_mode([a_info['rx_rssi'][i] for a_info in info_list])
		tx_rssi[i] = get_mode([a_info['tx_rssi'][i] for a_info in info_list])
		rx_rcpi[i] = get_mode([a_info['rx_rcpi'][i] for a_info in info_list])
		tx_rcpi[i] = get_mode([a_info['tx_rcpi'][i] for a_info in info_list])
		rx_snr[i] = get_mode([a_info['rx_snr'][i] for a_info in info_list])
		tx_snr[i] = get_mode([a_info['tx_snr'][i] for a_info in info_list])

	return {
			
		'tx_app_thrupt': tx_app_thrupt,
		'rx_app_thrupt': rx_app_thrupt,
		'tx_mac_loss': tx_mac_loss,
		'tx_mac_thrupt': tx_mac_thrupt,
		'rx_mac_thrupt': rx_mac_thrupt,
		'rx_rssi': rx_rssi,
		'tx_rssi': tx_rssi,
		'rx_rcpi': rx_rcpi,
		'tx_rcpi': tx_rcpi,
		'rx_snr' : rx_snr,
		'tx_snr': tx_snr
	
	}


def main():
	
	if len(sys.argv) < 4:
		usage()
		return -1

	tar_date = sys.argv[2] # get the target date
	tar_index = [i for i in sys.argv if sys.argv.index(i) > 2] # get all the target index

	folders = [f for f in os.listdir('log/')] # get all folders in log

	print(folders)

	all_rf_info = [] # use to store rf info

	for i in tar_index:
		
		tar_f = tar_date + '_' + str(i) # target folder name: date_index
		
		if tar_f not in folders:
			print('{} is not exists'.format(tar_f))
			return -1

		# open json file in target folder that store rf data in a round
		names = ['log', tar_f, tar_f + '.json']
		with open('/'.join(names), 'r') as jfile:
			rf_data = json.load(jfile)

		# process the rf data and get rf info that required to draw graph
		info = get_rf(rf_data)
		info['sw_option'] = sys.argv[1]
		all_rf_info.append(info)

		names[2] = tar_f + '_info.json'
		with open('/'.join(names), 'w') as outf:
			json.dump(info, outf)

	if len(tar_index) > 1:

		new_tar = tar_date + '_' + '-'.join(tar_index)
		os.mkdir('log/' + new_tar)

		names = ['log', new_tar, new_tar + '_info.json']
		with open('/'.join(names), 'w') as f_outf:
			json.dump(between_index(all_rf_info), f_outf)
			

if __name__ == '__main__':
	main()
